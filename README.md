### darpi 200

Service that returns  HTTP 200 to each request.
It listens at port 8200.

Use case: To test your HTTP client, to test your webservice script.


Written to learn what _darpi-rs_ is.


## See also

* [the darpi book](https://darpi-rs.github.io/book/)

* [darpi](https://github.com/darpi-rs/darpi)
