use darpi::{middleware, Body, Request};
use std::convert::Infallible;

#[middleware(Request)]
pub(crate) async fn roundtrip(
    #[request] _rp: &Request<Body>,
    #[handler] msg: impl AsRef<str> + Send + Sync + 'static,
) -> Result<String, Infallible> {
    let res = format!("{} from roundtrip middleware", msg.as_ref());
    Ok(res)
}
