mod handlers;
mod middleware;

use darpi::{app, logger::DefaultFormat, Method};
use darpi_middleware::{body_size_limit, compression::decompress};
use darpi_middleware::{log_request, log_response};
use handlers::*;

//todo assert middleware and job types to give more sensible errors
#[tokio::main]
async fn main() -> Result<(), darpi::Error> {
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    app!({
        address: "0.0.0.0:8200",
        // a set of global middleware that will be executed for every handler
        // the order matters and it's up to the user to apply them in desired order
        middleware: {
            request: [log_request(DefaultFormat), body_size_limit(128), decompress()],
            response: [log_response(DefaultFormat, request(0))]
        },
        //
        // edge case of using darpi-rs
        // darpi-ps has good "routing", this programs wants "wild cards".
        // (be prepared to see "duplicates")
        handlers: [
            {
                route: "/",
                method: Method::GET,
                handler: itisokay
            },
            {
                route: "/",
                method: Method::POST,
                handler: itisokay
            },
            {
                route: "/{p}",
                method: Method::GET,
                handler: itisokay_p
            },
            {
                route: "/{p}",
                method: Method::POST,
                handler: itisokay_p
            }
        ]
    })
    .run()
    .await
}
