use darpi::handler;
use http;

fn accept_okay(headers: &http::header::HeaderMap) -> String {
    let pa = headers.get("accept").unwrap().to_str().unwrap();
    if pa.starts_with("text/html") {
        // TODO set header "content-type: text/html; charset=utf-8"
        return "<html><head/><body><h1>OK</h1></body></html>\n".to_string();
    } else if pa.starts_with("application/json") {
        // TODO set header "content-type: application/json; charset=utf-8"
        return "{ \"retval\": \"OK\" }\n".to_string();
    } else {
        // "content-type: text/plain; charset=utf-8"
        return "It is OKay\n".to_string();
    }
}

#[handler]
pub(crate) async fn itisokay(#[request_parts] rp: &darpi::RequestParts) -> String {
    accept_okay(&rp.headers)
}

#[handler]
pub(crate) async fn itisokay_p(#[request_parts] rp: &darpi::RequestParts) -> String {
    accept_okay(&rp.headers)
}
